DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS system;
DROP TABLE IF EXISTS address_book;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS person;


CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id)
);


Create Table system(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(1000) NULL,
    owner_id INTEGER NOT NULL,
    FOREIGN KEY (owner_id) REFERENCES user(id)
);

Create Table address_book(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(1000) NOT NULL,
    description VARCHAR(1000) NOT NULL
);

Create Table person(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name CHAR(255) NOT NULL,
    last_name CHAR(255) NOT NULL,
    address VARCHAR(1000) NOT NULL,
    city VARCHAR(1000) NOT NULL,
    state VARCHAR(1000) NOT NULL,
    zip VARCHAR(100) NOT NULL,
    phone_number INTEGER(11) NOT NULL,
    address_book_id INTEGER NOT NULL,
    FOREIGN KEY (address_book_id) REFERENCES address_book (id)
);

