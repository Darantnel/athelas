from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
import sys
import json
from bson import json_util
from datetime import datetime
from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('address_book', __name__, url_prefix='/address_book')


@bp.route('/')
def index():
    db = get_db()
    address_books = db.execute(
        'SELECT * FROM address_book'
    ).fetchall()
    # print(json.dumps([tuple(row) for row in posts], default=json_util.default), file=sys.stderr)
    return render_template('address_book/index.html', address_books=address_books)


@bp.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == "POST":
        name = request.form['name']
        description = request.form['description']
        db = get_db()
        db.execute("insert into address_book (name, description) values (?,?)", (name, description))
        db.commit()
        return redirect(url_for('address_book.index'))

    return render_template('address_book/create.html')


@bp.route('/<int:id>/get_address_book', methods=('GET', 'POST'))
def get_address_book(id):
    db = get_db()
    address_book = db.execute('select name, description from address_book where id = ?', (id,)).fetchone()
    return address_book


@bp.route('/<int:id>/update%<string:message>', methods=('GET', 'POST'))
def update(id, message):
    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']
        db = get_db()
        db.execute('update address_book set name = ?, description=? where id = ?', (name, description, id))
        db.commit()
        return redirect(url_for('address_book.update', id=id, message="Updated Successfully"))
    address_book = get_address_book(id)
    return render_template('address_book/update.html', address_book=address_book, message=message)


@bp.route('/<int:id>/delete', methods=('GET', 'POST'))
def delete(id):
    address_book = get_address_book(id)
    if request.method == "POST":
        db = get_db()
        db.execute('delete from person where address_book_id = ?', (id,))
        db.commit()
        db.execute('delete from address_book where id = ?', (id,))
        db.commit()
        return redirect(url_for('address_book.index'))
    return render_template('address_book/delete.html',  address_book=address_book)



@bp.route('/<int:id>/view_persons')
def view_persons(id):
    # print(id, file=sys.stderr)
    db = get_db()
    persons = db.execute(
        'SELECT * FROM person where address_book_id= ?', (id,)
    ).fetchall()
    address_book = db.execute(
        'SELECT * FROM address_book where id= ?', (id,)
    ).fetchone()
    # print(json.dumps([tuple(row) for row in persons], default=json_util.default), file=sys.stderr)
    return render_template('address_book/view_persons.html', persons=persons, address_book=address_book)








