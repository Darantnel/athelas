from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
import sys
import json
from bson import json_util
from datetime import datetime
from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('person', __name__, url_prefix='/person')


@bp.route('/<int:id>/')
# def index(id):
#     if request.method == "POST":
#         db = get_db()
#         address_book_id = id
#         persons = db.execute(
#             'SELECT * FROM person where address_book_id = ?', (address_book_id,)
#         ).fetchall()
#         # print(json.dumps([tuple(row) for row in posts], default=json_util.default), file=
#         return redirect(url_for('address_book.view_persons', id=address_book_id))
#     person = get_person()
#     return render_template('persons/index.html', persons=person)


@bp.route('/<int:id>/create', methods=('GET', 'POST'))
def create(id):
    if request.method == "POST":
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        address = request.form['address']
        city = request.form['city']
        state = request.form['state']
        zip = request.form['zip']
        phone_number = request.form['phone_number']
        address_book_id = id
        db = get_db()
        db.execute("insert into person (first_name, last_name, address, city, state, zip, phone_number, address_book_id)"
                   " values (?,?,?,?,?,?,?,?)", (first_name, last_name, address, city, state, zip, phone_number, address_book_id))
        db.commit()
        return redirect(url_for('address_book.view_persons', id=address_book_id))

    return render_template('person/create.html')


@bp.route('/<int:id>/get_person', methods=('GET', 'POST'))
def get_person(id):
    db = get_db()
    person = db.execute('select name, description from person where id = ?', (id,)).fetchone()
    return person


@bp.route('/<int:id>/update%<string:message>', methods=('GET', 'POST'))
def update(id, message):
    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']
        db = get_db()
        db.execute('update person set name = ?, description=? where id = ?', (name, description, id))
        db.commit()
        return redirect(url_for('person.update', id=id, message="Updated Successfully"))
    person = get_person(id)
    return render_template('person/update.html', person=person, message=message)


@bp.route('/<int:id>/delete', methods=('GET', 'POST'))
def delete(id):
    person = get_person(id)
    if request.method == "POST":
        db = get_db()
        db.execute('delete from person where id = ?', (id,))
        db.commit()
        return redirect(url_for('person.index'))
    return render_template('person/delete.html',  person=person)









