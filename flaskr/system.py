from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
import sys
import json
from bson import json_util
from datetime import datetime
from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.db import get_db

bp = Blueprint('system', __name__)


@bp.route('/system')
def index():
    db = get_db()
    systems = db.execute("select * from system").fetchall()
    print(g, file=sys.stderr)
    print(json.dumps([tuple(row) for row in systems], default=json_util.default), file=sys.stderr)
    return render_template('system/index.html', post=systems)


def create():
    if request.method == "POST":
        name = request.form['name']
        description = request.form['description']
        owner_id = request.form['owner_id']

        db = get_db()
        db.execute("insert into system values (?,?,?)",
                   name, description, owner_id
               )
        db.commit()
        return redirect(url_for('system.index'))

    return render_template('system/create.html')